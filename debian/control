Source: pythonmagick
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Vcs-Git: https://salsa.debian.org/python-team/packages/pythonmagick.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pythonmagick
Uploaders: Carl Fürstenberg <azatoth@gmail.com>,
           Bastien Roucariès <rouca@debian.org>,
           Emmanuel Arias <eamanu@yaerobi.com>,
Build-Depends: autoconf-archive (>= 20160916),
               debhelper-compat (= 13),
               dh-python,
               libboost-python-dev (>= 1.34.1-8),
               libmagick++-dev (>= 8:7.1.1.39~),
               libtool (>= 1.5),
               pkg-config,
               python3-all,
               python3-all-dev,
               python3-setuptools,
Standards-Version: 4.6.1.1
Homepage: https://www.imagemagick.org
Rules-Requires-Root: no

Package: python3-pythonmagick
Architecture: any
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
Description: Object-oriented Python 3 interface to ImageMagick
 PythonMagick an interface to ImageMagick to give all the functionality
 of ImageMagick++ into Python 3.
 .
 ImageMagick is a software suite to create, edit, and compose bitmap images.
 It can read, convert and write images in a variety of formats (over 100)
 including DPX, EXR, GIF, JPEG, JPEG-2000, PDF, PhotoCD, PNG, Postscript,
 SVG, and TIFF. Use ImageMagick to translate, flip, mirror, rotate, scale,
 shear and transform images, adjust image colors, apply various special
 effects, or draw text, lines, polygons, ellipses and Bézier curves.
 All manipulations can be achieved through shell commands as well as through
 an X11 graphical interface (display).
